# README #

A simple class to divide a given MongoDB collection of documents to fairly equal chunks.

This was implemented in order to avoid skip() operation, since skip() performs full collection scan. 

Works by searching (binary search) for an ObjectId with a suitable timestamp, so number of documents between two ObjectIds would be roughly equal to the desired chunk size.
### Usage ###


```
#!python

from pymongo import MongoClient
from chunk import Chunk()

client = MongoClient()
db = client.some_db

chunk = Chunk()

chunks = chunk.get_chunks(db.some_large_collection, 100, 1000, verbose=True) # divide by 100 chunks with varying sizes +/-1000 documents, and output information

print(chunks)
# returns list of tuples of (objectid_from, objectid_to, number_of_objects)
# for interval objectid_from inclusive, objectid_to exclusive

```


### Performance ###

Dividing a fairly large collection using skip() can take quite long time. For example, for a collection with 50 million documents (1.3-2 KB each) diving to 100 chunks can take forever.
With binary search it takes 5-8 mins on my machine.


### Caveats ###

The algorithm guarantees that the last chunk would be no larger than total_documents/chunk_number + error, however there is no guarantee on the lower bound.