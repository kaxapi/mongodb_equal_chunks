from pymongo import MongoClient
import time
import datetime
from bson.objectid import ObjectId


class TooLowErrorInterval(Exception):
	pass


class Chunk():


	def __init__(self):
		pass


	def get_chunks(self, coll, num_chunks, max_error=1000, verbose=False):

		# get range of timestamps in the collection
		oid_start = coll.find_one({}, {'_id':1}).get('_id').generation_time.timestamp()
		oid_final = coll.find({}, {'_id':1}).sort('$natural',-1).limit(1)[0].get('_id').generation_time.timestamp()

		# calculate chunk size and initial step size
		chunk_size = coll.count() / num_chunks
		init_step = (oid_final - oid_start) / num_chunks

		# chunk's lower and upper bound 
		lower = chunk_size - max_error
		higher = chunk_size + max_error

		results = []
		oid_end = oid_start

		if verbose:
			print("lower = %d, higher = %d" % (lower, higher))

		while oid_end < oid_final:
			count = 0
			step = init_step

			while (not lower < count < higher) and oid_end < oid_final:
				oid_end += step
				count = coll.count({'_id': {'$gte': self.id_from_ts(oid_start), '$lt': self.id_from_ts(oid_end)}})
				if (count > higher and step > 0) or (count < lower and step < 0):
					if verbose:
						print('changing step to %d, count = %d' % (step/2, count))

					step = -step
					step = step/2
					if abs(step) < 1:
						raise TooLowErrorInterval("Too many objects for 1 second interval - increase max_error")

			results.append((self.id_from_ts(oid_start), self.id_from_ts(oid_end), count, ))
			oid_start = oid_end

		# replace last result with the timestamp of last object +1 second (since the pricision of ObjectId's timestamp is 1 second)
		results[-1] = (results[-1][0], self.id_from_ts(oid_final + 1), results[-1][2],)
		return results


	def id_from_ts(self, ts):
		return ObjectId.from_datetime(datetime.datetime.fromtimestamp(ts))


